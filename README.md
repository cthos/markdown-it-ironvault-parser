# markdown-it-ironvault-parser

Unofficial markdown-it plugin that will parse the KDL blocks produced by the [IronVault Obsidian plugin](https://github.com/iron-vault-plugin/iron-vault).

> ⚠️ This is a work in progress, and was largely just made to facilitate my Ironsworn / Starforged Campaign site needs (and [11ty starter](https://gitlab.com/cthos/11ty-solo-ttrpg-starter)). It could go poof at any time!

## Currently Supports

### Mechanics

This is the [Mechanics](https://iron-vault-plugin.github.io/iron-vault/blocks/mechanics-blocks.html) block, and my current focus.

* Rolls
* Progress Rolls
* Meters
* "-" nodes (which are treated as comments/blockquotes)
* Oracle Rolls
* Tracks being added / removed.

### Characters

* Character Stats
* Character Description / Details

### Todo

- [ ] Integrate Datasworn to properly pull info about iron-vault-character-assets
- [ ] iron-vault-character-meters
- [ ] iron-vault-character-special-tracks
- [ ] iron-vault-character-impacts
- [ ] iron-vault-character-assets

### Usage

Currently, if you're using require syntax, things are a little fuzzy:

```js
.use(require("@cthos/markdown-it-ironvault-parser").default)
```
