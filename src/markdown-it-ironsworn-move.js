import kdl from "kdljs";
import nunjucks from "nunjucks";
import _ from "lodash";
import path from "path";
import { fileURLToPath } from "url";
import { env } from "process";

const __dirname = path.dirname(fileURLToPath(import.meta.url));

export class IronswornMarkdownParser {
  templates;

  constructor(md, options) {
    // TODO: let you pass in different templates if this is a real plugin.
    this.templates = {
      move: path.join(__dirname, "templates/move.njk"),
      meter: path.join(__dirname, "templates/meter.njk"),
      comment: path.join(__dirname, "templates/comment.njk"),
      container: path.join(__dirname, "templates/block_container.njk"),
      progress: path.join(__dirname, "templates/progress.njk"),
      oracle: path.join(__dirname, "templates/oracle.njk"),
      track: path.join(__dirname, "templates/track.njk"),
      character_info: path.join(__dirname, "templates/character_info.njk"),
      character_stats: path.join(__dirname, "templates/character_stats.njk"),
      character_special_tracks: path.join(
        __dirname,
        "templates/character_special_tracks.njk",
      ),
    };

    if (options && options.templates) {
      this.templates = { ...this.templates, ...options.templates };
    }

    const config_directories = _.uniq(
      Object.values(this.templates).map((t) => {
        return path.dirname(t);
      }),
    );

    md.core.ruler.push("markdown-it-iron-vault-parser", function (state) {
      state.tokens = state.tokens.map((token) => {
        if (token.info.indexOf("iron-vault") < 0) {
          // ignore anything that doesn't have an iron-vault somewhere in the token name'
          return token;
        }

        if (token.info.indexOf("iron-vault-character") === 0) {
          token.type = "ironsworn_character";
          return token;
        }

        token.type = "ironsworn_result";
        return token;
      });

      nunjucks.configure(config_directories);

      return true;
    });

    md.renderer.rules["ironsworn_character"] = (
      tokens,
      idx,
      options,
      env,
      self,
    ) => {
      const token = tokens[idx];

      if (!env["iron-vault-kind"] || env["iron-vault-kind"] !== "character") {
        // if there's no iron-vault-kind data in the env, we can't parse this.
        return "";
      }

      let rendered_output = "";

      switch (token.info) {
        case "iron-vault-character-info":
          rendered_output += this.renderCharacterInfo(env);
          break;
        case "iron-vault-character-stats":
          rendered_output += this.renderCharacterStats(env);
          break;
        case "iron-vault-character-special-tracks":
          rendered_output += this.renderCharacterSpecialTracks(env);
          break;
      }

      return rendered_output;
    };

    md.renderer.rules["ironsworn_result"] = (
      tokens,
      idx,
      options,
      env,
      self,
    ) => {
      const token = tokens[idx];
      const kdlData = kdl.parse(token.content);

      if (!kdlData.output) {
        return "";
      }

      const contents = kdlData.output.reduce((rendered_output, dataBlock) => {
        switch (dataBlock.name) {
          case "move":
            rendered_output += this.renderMove(dataBlock);
            break;
          case "progress-roll":
            rendered_output += this.renderProgressRoll(dataBlock);
            break;
          case "meter":
            rendered_output += this.renderMeter(dataBlock);
            break;
          case "oracle":
            rendered_output += this.renderOracle(dataBlock);
            break;
          case "oracle-group":
            rendered_output += this.renderOracleGroups(dataBlock);
            break;
          case "track":
            rendered_output += this.renderTrack(dataBlock);
            break;
          case "-":
            rendered_output += this.renderComment(dataBlock);
            break;
        }

        return rendered_output;
      }, "");

      return nunjucks.render(this.templates.container, { contents });
    };
  }

  renderCharacterInfo(charData) {
    return nunjucks.render(this.templates.character_info, charData);
  }

  renderCharacterStats(charData) {
    return nunjucks.render(this.templates.character_stats, charData);
  }

  renderCharacterSpecialTracks(charData) {
    let templateData = charData;

    if (templateData.Quests_Progress !== undefined) {
      const questXp = this.getProgressBoxMap(
        parseInt(charData.Quests_Progress),
      );
      templateData = { ...templateData, questXp };
    }

    if (templateData.Bonds_Progress !== undefined) {
      const bondsXp = this.getProgressBoxMap(parseInt(charData.Bonds_Progress));
      templateData = { ...templateData, bondsXp };
    }

    if (templateData.Discoveries_Progress !== undefined) {
      const discXp = this.getProgressBoxMap(
        parseInt(charData.Discoveries_Progress),
      );
      templateData = { ...templateData, discXp };
    }

    return nunjucks.render(
      this.templates.character_special_tracks,
      templateData,
    );
  }

  getProgressBoxMap(trackValue, trackBasis = 4) {
    const boxesToFill = Math.floor(trackValue / trackBasis);
    const lastBoxProgress = trackValue % trackBasis;
    const boxArr = new Array(10).fill("blank");

    for (let i = 0; i < boxesToFill; i++) {
      boxArr[i] = "full";
    }
    if (lastBoxProgress) {
      boxArr[boxesToFill] = `progress-${lastBoxProgress}`;
    }

    return boxArr;
  }

  renderComment(dataBlock) {
    return nunjucks.render(this.templates.comment, {
      comment: dataBlock.values.join(""),
    });
  }

  /**
   * Renders the "Meter" syntax, when a stat goes from one value to another.
   *
   * @param {DataBlock} dataBlock
   * @returns
   */
  renderMeter(dataBlock) {
    const blockTitle = dataBlock.values.join(" / ");

    const delta = dataBlock.properties.to - dataBlock.properties.from;
    const isNeg = delta <= 0;

    const context = {
      blockTitle,
      properties: dataBlock.properties,
      delta,
      isNeg,
    };

    return nunjucks.render(this.templates.meter, context);
  }

  /**
   * Renders oracle blocks.
   */
  renderOracle(dataBlock) {
    const blockTitle = dataBlock.properties.name.match(/\[(.*?)\]/)[1];

    return nunjucks.render(this.templates.oracle, {
      blockTitle,
      roll: dataBlock.properties.roll,
      result: dataBlock.properties.result,
    });
  }

  /**
   * Grabs oracle groups and renders them together.
   */
  renderOracleGroups(dataBlock) {
    if (!dataBlock.children) {
      return "";
    }

    return dataBlock.children
      .map((oracle) => this.renderOracle(oracle))
      .join("\n");
  }

  /**
   * Renders the "track" data block.
   *
   * @param {DataBlock} dataBlock
   */
  renderTrack(dataBlock) {
    const blockTitle = dataBlock.properties.name.match(/\[.*?\|(.*?)\]/)[1];

    return nunjucks.render(this.templates.track, {
      blockTitle,
      action: dataBlock.properties.status,
    });
  }

  /**
   * Renders the Move block which optionally has rolls attached to it as sub blocks.
   * You can theoretically have _any_ sub block you want in here and that's valid
   * syntax, but this only renders rolls.
   *
   * @param {DataBlock} dataBlock
   * @returns
   */
  renderMove(dataBlock) {
    const blockTitle = dataBlock.values
      .map((v) => {
        return v.match(/\[(.*?)\]/)[1];
      })
      .join(" / ");

    const adds = dataBlock.children?.filter((a) => a.name == "add");
    const roll = dataBlock.children?.find((a) => a.name == "roll");

    let rollstatus = "";

    if (roll) {
      roll.total =
        roll.properties.adds + roll.properties.stat + roll.properties.action;
      rollstatus = this.calcRollStatus(roll);
    }

    const context = {
      blockTitle,
      dataBlock,
      roll,
      adds,
      rollstatus,
    };

    return nunjucks.render(this.templates.move, context);
  }

  /**
   * Renders a progress-roll block.
   *
   * @param {*} dataBlock
   * @returns
   */
  renderProgressRoll(dataBlock) {
    const roll = {
      total: dataBlock.properties.score,
    };

    roll.properties = dataBlock.properties;

    let rollstatus = this.calcRollStatus(roll);

    const context = {
      dataBlock,
      roll,
      rollstatus,
    };

    return nunjucks.render(this.templates.progress, context);
  }

  /**
   * TODO: This is a string due to nunjucks lack of conditionals. Filter? Maybe filter.
   *
   * @param {*} roll
   * @returns
   */
  calcRollStatus(roll) {
    if (roll.total > roll.properties.vs1 && roll.total > roll.properties.vs2) {
      return "Success";
    } else if (
      roll.total > roll.properties.vs1 ||
      roll.total > roll.properties.vs2
    ) {
      return "Partial Success";
    }

    return "Failure";
  }
}

export default function IronswornMarkdownIt(md, _options) {
  return new IronswornMarkdownParser(md, _options);
}
