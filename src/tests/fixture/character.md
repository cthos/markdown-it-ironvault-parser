---
name: Lyric
xp_spent: 0
momentum: 6
edge: 1
heart: 3
iron: 1
shadow: 2
wits: 2
health: 5
spirit: 5
supply: 3
Quests_Progress: 7
Quests_XPEarned: 2
Bonds_Progress: 1
Bonds_XPEarned: 0
Discoveries_Progress: 3
Discoveries_XPEarned: 0
iron-vault-kind: character
callsign: Baba
pronouns: She/Her
assets:
  - id: asset:starforged/command_vehicle/starship
    abilities:
      - true
      - false
      - false
    controls:
      integrity: 5
      integrity/battered: false
      integrity/cursed: false
    options:
      name: Asclepius
  - id: asset:starforged/companion/glowcat
    abilitiescampaign:
      - true
      - false
      - false
    controls:
      health: 3
      health/out_of_action: false
    options:
      name: Princess
---


```iron-vault-character-info
```

```iron-vault-character-stats
```

```iron-vault-character-meters
```

```iron-vault-character-special-tracks
```

```iron-vault-character-impacts
```

```iron-vault-character-assets
```


## Background

A lost generation ship, sent out before the machine wars has finally arrived in the forge. Unfortunately, the only surviving passenger was Lyric - years of catastrophes slowly decimated the population, and Lyric was the last child born to the ship. Her parents died a few years before the ship reached the forge. The on-board AI kept the ship limping along until the final phase of the journey, finally failing days before arriving in the forge.

A group of salvagers found the floating hulk and were astonished to find the young Lyric tending to the graves of everyone who had died on the long journey. Among the group was a member of the Healer’s guild who offered to take Lyric in. She’d already spent the last several years tending to her parents, both of whom had been critically injured because of one of the ship’s many malfunctions - so it was a natural transition for her.

Years later, when looking through the archives, she discovered that there was another generation ship launched a few years after hers. She wants to find it, hoping that it fared better on the journey than hers did. So, her background vow is:

> I will find the Generation Ship Valkyrie, and save anyone who is left to save.

## Contacts

- [Rucks](app://obsidian.md/Rucks)
