---
name: Lyric
xp_spent: 0
momentum: 6
edge: 1
heart: 3
iron: 1
shadow: 2
wits: 2
health: 5
spirit: 5
supply: 3
iron-vault-kind: character
callsign: Baba
pronouns: She/Her
assets:
  - id: asset:starforged/command_vehicle/starship
    abilities:
      - true
      - false
      - false
    controls:
      integrity: 5
      integrity/battered: false
      integrity/cursed: false
    options:
      name: Asclepius
  - id: asset:starforged/companion/glowcat
    abilitiescampaign:
      - true
      - false
      - false
    controls:
      health: 3
      health/out_of_action: false
    options:
      name: Princess
---


```iron-vault-character-info
```

```iron-vault-character-stats
```

```iron-vault-character-meters
```

```iron-vault-character-special-tracks
```

```iron-vault-character-impacts
```

```iron-vault-character-assets
```
