# This has other markdown in it

I very much like paragraph tags.

```iron-vault-mechanics
move "[Heal](move:starforged\/moves\/recover\/heal)" {
    add 1 "Healer"
    roll "wits" action=5 adds=1 stat=2 vs1=1 vs2=8
}
meter "supply" from=5 to=3
meter "momentum" from=2 to=3
```

Awesome.
