```iron-vault-mechanics
move "[Heal](move:starforged\/moves\/recover\/heal)" {
    add 1 "Healer"
    roll "wits" action=5 adds=1 stat=2 vs1=1 vs2=8
}
```
