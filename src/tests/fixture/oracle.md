```iron-vault-mechanics
oracle name="[Settlement Oracles \/ Settlement Name](oracle:classic\/oracles\/settlement\/name)" result="__A feature of the landscape.__ Envision what it is. What makes it unusual or distinctive?" roll=11
```