```iron-vault-mechanics
oracle-group name="Action and Theme Oracles: New Action and Theme Oracles" {
    oracle name="[Action and Theme Oracles \/ Action](oracle_rollable:classic\/action_and_theme\/action)" result="Coordinate" roll=55
    oracle name="[Action and Theme Oracles \/ Theme](oracle_rollable:classic\/action_and_theme\/theme)" result="Religion" roll=83
}
```