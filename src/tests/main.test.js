import MarkdownIt from "markdown-it";
import IronswornMarkdownIt, {
  IronswornMarkdownParser,
} from "../markdown-it-ironsworn-move";
import fs from "fs";
import path from "path";
import matter from "gray-matter";
import { fileURLToPath } from "url";

const __dirname = path.dirname(fileURLToPath(import.meta.url));

describe("Basics", () => {
  it("properly patches MarkdownIt", () => {
    const md = new MarkdownIt();
    md.use(IronswornMarkdownIt);

    expect(md.renderer.rules["ironsworn_result"]).toBeDefined();
    // It says not to use this directly but they don't actually provide a way to go grab the rule by name directly.
    expect(
      md.core.ruler.__find__("markdown-it-iron-vault-parser"),
    ).toBeGreaterThan(-1);
  });
});

describe("Parser Properly Parses Content", () => {
  let md;

  beforeEach(() => {
    md = new MarkdownIt();
    md.use(IronswornMarkdownIt);
  });

  /**
   * Simply checks to see if the HTML contains the processing template.
   */
  it("Processes Move Blocks", () => {
    const fileContents = fs.readFileSync(
      path.join(__dirname, "fixture/move.md"),
      "utf-8",
    );

    const processedHtml = md.render(fileContents);
    expect(processedHtml).toContain("ironsworn-move-adds");
  });

  it("Processes Meter Blocks", () => {
    const fileContents = fs.readFileSync(
      path.join(__dirname, "fixture/meter.md"),
      "utf-8",
    );

    const processedHtml = md.render(fileContents);
    expect(processedHtml).toContain("ironsworn-meter");
    expect(processedHtml).toContain("momentum");
  });

  it("Processes Progress Roll Blocks", () => {
    const fileContents = fs.readFileSync(
      path.join(__dirname, "fixture/progress.md"),
      "utf-8",
    );

    const processedHtml = md.render(fileContents);
    expect(processedHtml).toContain("ironsworn-progress-roll");
    expect(processedHtml).toContain("6");
  });

  it("Processes Oracle Blocks", () => {
    const fileContents = fs.readFileSync(
      path.join(__dirname, "fixture/oracle.md"),
      "utf-8",
    );

    const processedHtml = md.render(fileContents);
    expect(processedHtml).toContain("ironsworn-oracle");
    expect(processedHtml).toContain("feature");
  });

  it("Processes Oracle Group Blocks", () => {
    const fileContents = fs.readFileSync(
      path.join(__dirname, "fixture/oracle_group.md"),
      "utf-8",
    );

    const processedHtml = md.render(fileContents);
    expect(processedHtml).toContain("ironsworn-oracle");
    expect(processedHtml).toContain("Coordinate");
    expect(processedHtml).toContain("Religion");
  });

  it("Processes Track Blocks", () => {
    const fileContents = fs.readFileSync(
      path.join(__dirname, "fixture/track.md"),
      "utf-8",
    );

    const processedHtml = md.render(fileContents);
    expect(processedHtml).toContain("ironsworn-track");
    expect(processedHtml).toContain("Tomb");
  });

  it("Processes - Blocks", () => {
    const fileContents = fs.readFileSync(
      path.join(__dirname, "fixture/comment.md"),
      "utf-8",
    );

    const processedHtml = md.render(fileContents);
    expect(processedHtml).toContain("ironsworn-comment");
    expect(processedHtml).toContain("I roll +Heart");
  });
});

describe("Properly Parses Character info", () => {
  let md;

  beforeEach(() => {
    md = new MarkdownIt();
    md.use(IronswornMarkdownIt);
  });

  it("Parses Character Blocks", () => {
    const fileContents = fs.readFileSync(
      path.join(__dirname, "fixture/character.md"),
      "utf-8",
    );

    const frontmatter = matter(fileContents);
    const processedHtml = md.render(fileContents, frontmatter.data);
    expect(processedHtml).toContain("Lyric");
    expect(processedHtml).toContain("Baba");
    expect(processedHtml).toContain("She/Her");
  });

  it("Properly parses character stats", () => {
    const fileContents = fs.readFileSync(
      path.join(__dirname, "fixture/character.md"),
      "utf-8",
    );

    const frontmatter = matter(fileContents);
    const processedHtml = md.render(fileContents, frontmatter.data);
    expect(processedHtml).toContain("Edge");
  });

  it("Properly parses character special tracks", () => {
    const fileContents = fs.readFileSync(
      path.join(__dirname, "fixture/character.md"),
      "utf-8",
    );

    const frontmatter = matter(fileContents);
    const processedHtml = md.render(fileContents, frontmatter.data);
    expect(processedHtml).toContain("Quest XP:");
    expect(processedHtml).toContain("progress-1");
    expect(processedHtml).toContain("progress-3");
  });

  it("Doesn't crash if track is missing", () => {
    const fileContents = fs.readFileSync(
      path.join(__dirname, "fixture/character_missing_tracks.md"),
      "utf-8",
    );

    const frontmatter = matter(fileContents);
    const processedHtml = md.render(fileContents, frontmatter.data);
    expect(processedHtml).not.toContain("Quest XP:");
    expect(processedHtml).not.toContain("progress-1");
  });
});
